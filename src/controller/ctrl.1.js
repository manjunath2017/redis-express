const user = require("../model/user");
// const ObjectId = require("mongoose").Types.ObjectId;
// const _ = require("lodash");
// const validator = require('express-validator');
const redis = require('redis');

//Retrieve all record
module.exports.getData = async (request, response) => {
 
    const result = await user.find({}); //  .sort({ _id: -1 }); //.sort -> sorts from ascending order
    response.send(result);
}
// retrieve single record
module.exports.getDataById = async (request, response) => {
  // var id = request.params.getByid;
  // console.log(id);

  // // check the ObjectId format
  // if (!ObjectId.isValid(id)) {
  //   return response.status(400).send({
  //     message: `Invalid ID:${id}!` //
  //   });
  // }
  // user
  //   .findById(id)
  //   .then(result => {
  //     if (!result) {
  //       return response.status(404).send({
  //         message: `Id:${id} not found!-`
  //       });
  //     }
  //     response.send(result);
  //   })
  //   .catch(error => {
  //     response.status(400).send(error);
  //   });
  // try {
  //   const redis = require('redis');
  //   const redisUrl = 'redis://127.0.0.1.6379';

  // var redis = require('redis');
  // var client = redis.createClient( {host : 'localhost', port : 6379} );
  // var client = redis.createClient( {host : 'localhost', port : 6379} );
  // const redisUrl = 'redis://127.0.0.1.6379';
  // const client = redis.createClient(redisUrl);

  var client = redis.createClient({ host: 'localhost', port: 6379 });
  // var client = redis.createClient(6380, "localhost");

  const util = require('util');
  client.get = util.promisify(client.get);

  // var client = redis.createClient();

  client.on('connect', () => {
    console.log('Redis client connected-------- \n \n \n \n');
  });

  client.on('error', (err) => {
    console.log('Something went wrong ' + err);
  });

  // client.set('my test key', 'my test value', redis.print);
  // client.get('my test key',  (error, result) => {
  //     if (error) {
  //         console.log(error);
  //         throw error;
  //     }
  //     console.log('GET result ->' + result);
  // });

  // const client = redis.createClient(redisUrl);


  

  // const cachedApi = await client.get(request.params.getByid );
  // if (cachedApi){
  //   console.log(' Form Cache!!!', JSON.stringify(cachedApi) );
  //   response.send(JSON.stringify(cachedApi) );
  // }
  
  // // https://github.com/kephin/Node_Redis-Caching
 
  // else{
    const result = await user.findById(request.params.getByid);
  //   console.log('Served from MongoDB', JSON.stringify(result));
    response.send((result) );
  //   client.set(request.params.getByid, JSON.stringify(result)); 
  // }
};