const user = require("../model/user");
const ObjectId = require("mongoose").Types.ObjectId;
const _ = require("lodash");
const validator = require('express-validator');

//Retrieve all record
module.exports.getData = async (request, response) => {
  // working....
  // user
  //   .find({})
  //   .sort({ _id: -1 })
  //   .then(result => {
  //     response.send(result);
  //   })
  //   .catch(error => {
  //     console.log(error);
  //
  try {
    const result = await user.find({}).sort({ _id: -1 }); //.sort -> sorts from ascending order
    response.send(result);
  } catch (error) {
    response.status(500).send(error);
  }
};
// retrieve single record
module.exports.getDataById = async (request, response) => {
  var id = request.params.getByid;
  // check the ObjectId format
  if (!ObjectId.isValid(id)) {
    return response.status(400).send({
      message: `Invalid ID:${id}!` //
    });
  }
  // user
  //   .findById(id)
  //   .then(result => {
  //     if (!result) {
  //       return response.status(404).send({
  //         message: `Id:${id} not found!-`
  //       });
  //     }
  //     response.send(result);
  //   })
  //   .catch(error => {
  //     response.status(400).send(error);
  //   });
  try {
    const result = await user.findById(id);
    if (!result) {
      return response.status(404).send({
        message: `Id:${id} Not found []`
      });
    }
    response.send(result);
  } catch (error) {
    response.status(500).send(error);
  }
};
//Create Record
module.exports.postData = async (request, response) => {
  // var bodyData = _.pick(request.body, ["name", "school"]);
  // var getData = new user(bodyData); //call model
  //or
  var getData = new user(request.body);
  // var bodyData = new user({
  //     name:request.body.name,
  //     school:request.body.school
  // });

  //working...
  // getData
  //   .save()
  //   .then(result => {
  //     response.send(result);
  //   })
  //   .catch(error => {
  //     response.send(error);
  //   });

  try {
    await getData.save();
console.log()
    response.status(201).send(getData);
  } catch (error) {
    console.log('error.drive', error); 
    if (error.driver === true) { //error is in object compare only error.drive
      return response.status(500).send("Already name is existing!!");
    }
    if (error.name === "ValidationError") {
      return response.status(500).send("Fields cannot be blank!!");
    }
    response.status(500).send(error);
  }
};

//update
module.exports.editPost = async (request, response) => {
  const id = request.params.getByid;

  //update only specific fields
  const updates = Object.keys(request.body); //convert object to array
  const allowedUpdates = ["name", "school"]; // only this attributes can be updated
  const isValidOperation = updates.every(update =>
    allowedUpdates.includes(update)
  );
  console.log(isValidOperation);
  if (!isValidOperation) {
    return response.status(400).send({ error: `Invalid update` });
  }
  if (!ObjectId.isValid(id)) {
    return response.status(400).json({
      message: `_id:${id} not found!`
    });
  }
  var body = _.pick(request.body, ["name", "school"]);
  // user
  //   .findByIdAndUpdate(id, body, { new: true })
  //   .then(result => {
  //     if (!result) {
  //       return response.status(404).send({
  //         message: `_id : ${id} not found!-`
  //       });
  //     }
  //     response.send(result);
  //   })
  //   .catch(error => {
  //     return response.status(500).send({
  //       message: "Error updating note with id " + id + error
  //     });
  //   });
  try{
    const result = await user.findByIdAndUpdate(id, body, { new: true, runValidators:true });
    if(!result){
      return response.status(404).send({
        message:` _id:${id} not found!`
      })
    }
    response.send(result);
  }catch(error){
    response.status(500).send(error);
  }
};

//delete
module.exports.deleteById = (request, response) => {
  const id = request.params.getByid;
  //   console.log(request.params.getByid, request.params.key);

  if (!ObjectId.isValid(id)) {
    return response.status(404).send({
      message: `_id: ${id} not found`
    });
  }
  /*
    In 'findOneAndRemove' we have to pass attribute which we want to delete
    user.findOneAndRemove({name:"Manjunath"}).then(result=>{}).catch(error=>{})
  */
  // in findByIdAndRemove it will delete only by id
  user
    .findByIdAndRemove(id)
    .then(result => {
      if (!result) {
        return response.status(404).send({
          message: `_id: ${id} not found in result`
        });
      }
      response.send({
        message: `_id:${id} is successfully deleted`
      });
    })
    .catch(error => {
      if (
        error.kind === "ObjectId" ||
        error.name === "CastError" ||
        error.name === "NotFound"
      ) {
        return response.status(404).send({
          message: `_id:${id} Not found `
        });
      }
    });
};