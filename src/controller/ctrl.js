const user = require("../model/user"); 
const redis = require('redis');

//Retrieve all record
module.exports.getData = async (request, response) => {
  try {
    const result = await user.find({}).sort({ _id: -1 }); //.sort -> sorts from ascending order
    console.log(result);
    response.send(result);
  } catch (error) {
    response.status(500).send(error);
  }
};

// retrieve single record
module.exports.getDataById =   (request, response) => {
 const client = redis.createClient();
  client.get(request.params.getByid, async (err, result) => {

  if (result) {
    console.log('\n \n \n \n \n Cache!! \n \n \n \n \n', result);
    response.send(result);
  } 
  else  
    try {
      const result = await user.findById(request.params.getByid ); 
      console.log('MongoDB', result);
      client.set(request.params.getByid,  JSON.stringify(result));
      response.send(result);
    } catch (error) {
      response.status(500).send(error);
    }
 });
};
 