const mongoose=require('mongoose');
 

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        trim:true,
        required:true,
        unique:true
    },
    school:{
        type:String,
        trim:true,
        required:true
    },
});
module.exports=mongoose.model('userSchema', userSchema,'userCollection');