const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config();
const conn = mongoose.connect(
  process.env.DB_CONNECT,
  {
    useNewUrlParser: true,
    useCreateIndex: true, 
    useFindAndModify: false
  },
  error => {
    if (!error) {
      console.log(`DB connected SuccessFully!`);
    } else {
      console.log(`DB not connected ${error}`);
    }
  }
);

