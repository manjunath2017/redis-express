var express = require('express');
var router = express.Router();

const ctrl=require('../controller/ctrl');

router.get('/', ctrl.getData); 

router.get('/:getByid', ctrl.getDataById); 

module.exports=router;
