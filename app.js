const app=require('express')();
const bodyParser=require('body-parser');
app.use(bodyParser.json());

const responseTime = require('response-time')
app.use(responseTime());
// const chalk = require('chalk');
// console.log(chalk.blue.bgRed.bold('Hello world!'));

require('./src/DB/dbConnection');

//import Schema
require('./src/model/user');

//import router
const getRouter = require('./src/router/router');
app.use('/', getRouter);
// dotenv.config(); // --> is included in db.Connection.js page
 const port=3001;

app.listen(port);
/*
app.listen(process.env.PORT,(error) =>{
    if(!error) 
        return console.log(`Server running on port ${process.env.PORT}`);
    console.log(`error ${error}`);
*/
